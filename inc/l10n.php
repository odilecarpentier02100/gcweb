<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


$lang = array();

function load_l10n($domain) {
    global $lang, $conf;
    @include PATH_GCWEB.'/locales/'.$conf['lang'].'/'.$domain.'.php' ;
}

function load_template_l10n($domain,$lang_code='eng') {
    global $lang, $conf;
    if (file_exists(PATH_GCWEB.'templates/'.$conf['template'].'/locales/'.$lang_code.'/'.$domain.'.php'))
        include PATH_GCWEB.'templates/'.$conf['template'].'/locales/'.$lang_code.'/'.$domain.'.php';
    elseif (file_exists(PATH_GCWEB.'templates/default/locales/'.$lang_code.'/'.$domain.'.php'))
        include PATH_GCWEB.'templates/default/locales/'.$lang_code.'/'.$domain.'.php';
}


function __($str,$comment='') {
    global $lang;
    if (isset($lang[$str]))
        return $lang[$str];
    else
        return $str;
}
?>
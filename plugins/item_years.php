<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Ajout en champ "année de parution" en plus de celui de la date
 */

# collection type GCfilms
if ($collec['type'] == 'GCfilms') {
    if ($item['date'] != $conf['champVide'])
        $item['year'] = substr($item['date'],-4);
    else
        $item['year'] = $conf['champVide'];
}

# collection type GCbooks
if ($collec['type'] == 'GCbooks') {
    if ($item['publication'] != $conf['champVide'])
        $item['year'] = substr($item['publication'],-4);
    else
        $item['year'] = $conf['champVide'];
}

# collection type GCgames
if ($collec['type'] == 'GCgames') {
    if ($item['released'] != $conf['champVide'])
        $item['year'] = substr($item['released'],-4);
    else
        $item['year'] = $conf['champVide'];
}

# collection type GCcomics
if ($collec['type'] == 'GCcomics') {
    if ($item['publishdate'] != $conf['champVide'])
        $item['year'] = substr($item['publishdate'],-4);
    else
        $item['year'] = $conf['champVide'];
}
?>

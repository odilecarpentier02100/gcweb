<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Convertir des champs enregistrer dans GCstar sous forme de chaine en tableau
 */


switch ($collec['type']) {
    case 'GCfilms' :
        if (!is_array($item['actors']))
            $item['actors'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['actors']));
        if (!is_array($item['country']))
            $item['country'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['country']));
        if (!is_array($item['director']))
            $item['director'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['director']));
        if (!is_array($item['genre']))
            $item['genre'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['genre']));
        break;
    case 'GCmusics' :
        if (!is_array($item['artist']))
            $item['artist'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['artist']));
        break;
    case 'GCboardgames' :
        if (!is_array($item['designedby']))
            $item['designedby'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['designedby']));
        break;
    case 'GCcomics' :
        if (!is_array($item['writer']))
            $item['writer'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['writer']));
        if (!is_array($item['illustrator']))
            $item['illustrator'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['illustrator']));
        if (!is_array($item['colourist']))
            $item['colourist'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['colourist']));
        break;
    case 'GCbooks' :
        if (!is_array($item['authors']))
            $item['authors'] = explode('%252C',preg_replace('/(%20)*%252C(%20)*/', '%252C', $item['authors']));
        break;
}
?>
<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Partie centale de la page qui affiche les infomration détaillée d'un élement.
 * Variable disponible : $info, $collec, $item
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="item">
        <div class="nav">
            <div class="box">

                <?php $maxkey = count ($bdd) -1;

                //Items précédentes
                if ($item['key'] > 0) {
                    foreach (array_slice($bdd, max($item['key']-2, 0), min($item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['title']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['cover'],160,80) ?>" <?php aff_attrsize_image($tmpitem['cover'],160,80) ?> alt="title" />
                        </a><?php
                    }
                }

                //Item courant
                ?><a id="id_<?php aff($item['id']) ?>" title="<?php aff($item['title']) ?>" href="<?php aff_hrefitem($item)?>">
                <img class="image" src="<?php aff_image($item['cover'],240,120) ?>" <?php aff_attrsize_image($item['cover'],240,120) ?> alt="<?php aff($item['title']) ?>" />
                </a><?php

                //Item suivant
                if ($item['key'] < $maxkey) {
                    foreach (array_slice($bdd, min($item['key']+1, $maxkey), min($maxkey - $item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['title']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['cover'],160,80) ?>" <?php aff_attrsize_image($tmpitem['cover'],160,80) ?> alt="title" />
                        </a><?php
                    }
                }
                ?><br />

                <a title="Parcourir" href="<?php aff_hrefPage($item['key']/$conf['itemsPage']+.5) ?>#id_<?php aff($item['id']) ?>">↑</a> &nbsp;
                <?php
                aff_prevItem(5,' - ');
                aff_currentItem();
                aff_nextItem(5,' - ');
                ?>
            </div>
        </div>
        <div class="element">
            <div class="box">
                <span class="image">

                    <?php if (test($item['backpic'])) { ?>


                        <a href="<?php aff_image($item['cover']) ?>">
                        <img title="<?php echo __('Cliquez dans le coin inférieur droit pour voir une autre image') ?>" src="<?php aff_image($item['cover'],280,320) ?>" <?php aff_attrsize_image($item['cover'],280,320) ?> alt="<?php aff($item['title'])?>" />
                        </a>


                        <a id='img2' style="display:none" href="<?php aff_image($item['backpic'],280,320) ?>">
                        <img title="<?php echo __('Cliquez dans le coin inférieur droit pour voir une autre image') ?>" src="<?php aff_image($item['backpic'],280,320) ?>" <?php aff_attrsize_image($item['backpic'],280,320) ?>  alt="<?php aff($item['title'])?>" />
                        </a>
                        <a class='backpic' title="<?php echo __('Suivant') ?>" href="#" onclick="switchimg(2);return false;">↻</a>
                    <?php } else { ?>
                        <a id='img1' href="<?php aff_image($item['cover']) ?>">
                        <img src="<?php aff_image($item['cover'],280,320) ?>" <?php aff_attrsize_image($item['cover'],280,320) ?> alt="<?php aff($item['title'])?>" />
                        </a>
                    <?php } ?>

                </span>
                <h2><?php aff($item['title'])?></h2>

                <?php if (test($item['comments']))            {?> <p class="description"><?php aff($item['comments']) ?></p> <?php } ?>


                <span class="starNote"><?php aff_star($item['rating']) ?></span>

                <ul>
                    <?php if (test($item['artist']))    {?> <li><span class="label"><?php echo __('Artiste') ?> :</span>        <span class="info"><?php aff_filter('artist==',$item['artist']) ?></span></li><?php } ?>
                    <?php if (test($item['genre']))     {?> <li><span class="label"><?php echo __('Genre') ?> :</span>          <span class="info"><?php aff_filter('genre==',$item['genre']) ?></span></li><?php } ?>
                    <?php if (test($item['release']))   {?> <li><span class="label"><?php echo __('Sortie') ?> :</span>         <span class="info"><?php aff_filter('releaser==',$item['release']) ?></span></li><?php } ?>
                    <?php if (test($item['label']))     {?> <li><span class="label"><?php echo __('Label') ?> :</span>          <span class="info"><?php aff_filter('label==',$item['label']) ?></span></li><?php } ?>
                    <?php if (test($item['composer']))  {?> <li><span class="label"><?php echo __('Compositeur') ?> :</span>    <span class="info"><?php aff_filter('composer==',$item['composer']) ?></span></li><?php } ?>
                    <?php if (test($item['producer']))  {?> <li><span class="label"><?php echo __('Producteur') ?> :</span>     <span class="info"><?php aff_filter('producer==',$item['producer']) ?></span></li><?php } ?>
                    <?php if (test($item['format']))    {?> <li><span class="label"><?php echo __('Format') ?> :</span>         <span class="info"><?php aff_filter('format==',$item['format']) ?></span></li><?php } ?>
                    <?php if (test($item['origin']))    {?> <li><span class="label"><?php echo __('Origine') ?> :</span>        <span class="info"><?php aff_filter('origin==',$item['origin']) ?></span></li><?php } ?>
                    <?php if (test($item['location']))  {?> <li><span class="label"><?php echo __('Emplacement') ?> :</span>    <span class="info"><?php aff_filter('location==',$item['location']) ?></span></li><?php } ?>
                    <?php if (test($item['tags']))      {?> <li><span class="label"><?php echo __('Tags') ?> :</span>           <span class="info"><?php aff_filter('tags==',$item['tags']) ?></span></li><?php } ?>
                    <?php if (test($item['web']))       {?> <li><a href="<?php aff($item['web'],' - ','<br />')?>"><?php echo __('Lien web') ?></a></li><?php }

                        $counttrack = count($item['tracks'][0]);
                        if ($counttrack != 0) {
                    ?>
                     <li>
                        <table>
                            <tr>
                                <th colspan="<?php echo $counttrack-1 ?>"><?php echo count($item['tracks']) ?> pistes</th>
                                <th><?php if (test($item['running'])) aff($item['running']); ?></th>
                            </tr>
                            <?php if (test($item['tracks']))   {?>
                            <tr>
                                <td>
                                <?php aff($item['tracks'],"</td>\n\t\t\t<td>","</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>") ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                     </li>
                    <?php } ?>
                </ul>
                <?php if (isset($conf['commercialLink'])) {?>
                    <?php aff($conf['commercialLink']) ?>
                <?php }

                echo join("\n", $item['array_add_to_all_pages']);
                echo join("\n", $item['array_add_to_page_detail']);
                ?>
                <div class="foot">
                </div>
            </div>
        </div>
    </div>
</div>

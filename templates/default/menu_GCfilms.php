<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
?>
    <div id="menu">

        <div id="infoCollec" class="box">
            <h2><?php aff($collec['title']) ?></h2>
            <p class="description"><?php aff($collec['description']) ?></p>
        </div>

        <div id="navigation" class="box">
            <h2><?php echo __('Navigation') ?></h2>
            <ul>
                <li><a href="<?php echo URLRACINE_GCWEB ?>"><?php echo __('Page d\'accueil') ?></a></li>
                <li><a href="<?php aff_hrefModel('list') ?>"><?php echo __('Toute la collection') ?></a></li>
            </ul>
            <ul>
                <li><a href="<?php aff_hrefModel('cloud') ?>#genre"><?php echo __('Les genres') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#director"><?php echo __('Les réalisateurs') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#actors"><?php echo __('Les acteurs') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#country"><?php echo __('Pays') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#year"><?php echo __('Années de sortie') ?></a></li>
            </ul>
        </div>

        <div id="classer" class="box">
            <h2><?php echo __('Classer par') ?></h2>
            <ul>
                <li><?php echo __('Titre') ?>
                    <a <?php if (isSortKey('titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleASC') ?>">↓</a><a <?php if (isSortKey('titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Pays') ?>
                    <a <?php if (isSortKey('countryASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('countryASC') ?>">↓</a><a <?php if (isSortKey('countryDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('countryDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Réalisateur') ?>
                    <a <?php if (isSortKey('directorASC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('directorASC,titleASC') ?>">↓</a><a <?php if (isSortKey('directorDSC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('directorDSC,titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Notation') ?>
                    <a <?php if (isSortKey('ratingDSC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('ratingDSC,titleASC') ?>">↓</a><a <?php if (isSortKey('ratingASC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('ratingASC,titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Date d\'ajout') ?>
                    <a <?php if (isSortKey('addedDSC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('addedDSC,titleASC') ?>">↓</a><a <?php if (isSortKey('addedASC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('addedASC,titleDSC') ?>">↑</a>
                </li>
            </ul>
        </div>

        <div id="menusearch" class="box">
            <?php aff_search('start') ?>
            <h2><?php echo __('Chercher') ?></h2>
            <p><?php /*afficher la liste de chmps de recherche*/ aff_search('keyword') ?></p>
            <p><?php echo __('dans') ?> :
            <?php /*afficher la liste de chmps de recherche*/
                aff_search('in_champs', array(
                        array('title=%s,|original=%s,|director=%s,|actors=%s',__('tous')),
                        array('title=%s,|original=%s',__('titre')),
                        array('director=%s',__('réalisateur')),
                        array('actors_without_roles=%s',__('acteur')),
                    ));
            ?>
            </p>
            <p><?php aff_search('submit') ?></p>
            <?php aff_search('end') ?>
            <p><a href="<?php aff_hrefModel('search') ?>"><?php echo __('Recherche avancée') ?></a></p>
        </div>

        <div id="rss" class="box">
            <h2><?php echo __('S\'abonner') ?></h2>
            <ul class="rss">
                <li><a title="<?php echo __('S\'abonner au flux RSS') ?>" href="<?php aff_hrefModel('rss',False) ?>"><?php echo __('Toute la collection') ?></a></li>
                <li><a title="<?php echo __('S\'abonner au flux RSS') ?>" href="<?php aff_hrefModel('rss',True) ?>"><?php echo __('Éléments affichés') ?></a></li>
            </ul>
        </div>

        <div id="info" class="box">
            <h2><?php echo __('Informations') ?></h2>
            <ul>
                <li>
                <?php
                    if ($collec['nbItems'] == 0)    echo __('aucun élément affiché');
                    elseif ($collec['nbItems'] == 1)echo __('un élément affiché');
                    else                            printf(__('%d éléments affichés'),$collec['nbItems']);
                    echo '<br />';
                    $nbfiltre = $collec['nbItemsBDD'] - $collec['nbItems'];
                    if ($nbfiltre == 0)             echo __('aucun élément filtré');
                    elseif ($nbfiltre == 1)         echo __('un élément filtré');
                    else                            printf (__('%d éléments filtrés'),$nbfiltre);
                ?>
                </li>

                <li><?php printf (__('Collection mise à jour<br />le %s</li>'),strftime(__('%e/%m/%y'),filemtime(DIR_GCWEB.'collections/'.$collec['dir'].$collec['xml']))) ?>
            </ul>
        </div>

    </div>
    <!-- Fin du menu -->

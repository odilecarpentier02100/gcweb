#! /bin/sh

mypath=$(dirname $0)
cd "$mypath/../.."
mkdir -p locales/potfiles

deflist='main.php error.php head.php foot.php menuOptionsAff.php *GC*.php'
list2='*generator.php'

xgettext -k__ -kN__ --no-wrap --from-code UTF-8 $deflist -o locales/potfiles/default.pot
xgettext -k__ -kN__ --no-wrap --from-code UTF-8 $list2   -o locales/potfiles/generator.pot

for lang in "eng" "de" "spa" #add your language hier example : "eng" "de"
    do
    for file in default generator
        do
        echo $file $lang
        mkdir -p locales/$lang
        if [ -e locales/$lang/$file.po ]; then
            msgmerge -U locales/$lang/$file.po locales/potfiles/$file.pot
        else
            msginit -i locales/potfiles/$file.pot -o locales/$lang/$file.po
        fi
    done
done

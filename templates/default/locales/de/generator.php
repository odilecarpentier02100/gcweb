<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Un "nuage" (cloud) est réalisé avec l\'ensemble des valeurs d\'un champ.
    Plus la valeur est présente dans votre collection plus elle est mise en avant
    (exemple : le champ "auteur" affichera l\'ensemble des auteurs et mettera en
    avant celui dons vous procédez le plus d\'ouvrage).
    <a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#cloud">Plus d\'explications</a>.'
 => 'Eine "Cloud" wird erstellt aus den Werten eines Feldes.
    Je mehr ein Wert vorhanden ist, desto grösser wird dieser in der Cloud dargestellt.
    (zB: das Feld "Regisseur" zeigt alle Regisseure an und diejenigen von denen sie
    am meisten Filme besitzen, werden grösser dargestellt).
    <a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#cloud">mehr Informationen</a>.',
'Attention! Le modèle "menu" doit être créé avant cette page'
 => 'Achtung! Die Vorlage "menu" muss vor dieser Seite erstellt werden.',
'Liste des nuages'
 => 'Cloudliste',
'Titre du nuage'
 => 'Cloudtitel',
'champ avec lequel faire le nuage'
 => 'Feld für ein Cloudeintrag erstellen',
'Permet d\'exporter la liste des élément sous la forme d\'un fichier cvs
    (facillement importable dans un tableur type openoffice calc).
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#csv">Plus d\'explications</a>).</p>

    <p style="color:red">Attention la prévisualisation de cette page n\'est pas possible
    de plus lors de l\'enregistrement un message comme quoi la page contient des erreurs va
    s\'afficher. Celui-ci est sans importance.'
 => 'Ermöglicht es Ihnen, die Liste der Elemente als cvs-Datei zu exportieren
    (erleichtert das Importieren in einer Tabellenkalkulation wie OpenOffice Calc).
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#csv">mehr Informationen</a>).</p>

    <p style="color:red">Bitte beachten sie dass die Vorschau dieser Seite
    nicht möglich ist. Desweiteren wird eine Fehlermeldung beim speichern angezeigt.
    Diese ist ohne bedeutung.',
'Séléctionnez les champs devant être précédé par l\'url de votre site (tels que les images)'
 => 'Markieren Sie die Felder, denen die URL Ihrer Website vorangestellt werden soll (zB : Bilder)',
'Cette page affiche le détails d\'un élement (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#item">Plus d\'explications</a>)'
 => 'Diese Seite zeigt die Details eines Artikels an (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#item">mehr Informationen</a>)',
'Titre de l\'élément'
 => 'Titel des Objektes',
'Saisie obligatoire'
 => 'Erforderliche Angabe',
'Note'
 => 'Bewertung',
'(certainement un chiffre entre 0 et 10)'
 => '(Sicher eine Zahl zwischen 0 und 10)',
'Image représentative'
 => 'Beispielsbild',
'Autre image'
 => 'Anderes Bild',
'Taille des images en pixel (largeur max x hauteur max, les proportions sont conservées)'
 => 'Grösse der Bilder in Pixel (max. Breite x max. Höhe, die Proportionen bleiben beibehalten)',
'de l\'élément courant dans la barre de navigation'
 => 'das aktuelle Objekt im Navigationsmenü',
'Valeurs conseillées pour une image plus haute que large'
 => 'Empfohlene Werte für ein Bild welches höher als breiter ist',
'Les proportions de l\'image seront conservées'
 => 'Die Proportionen des Bildes bleiben erhalten',
'des autres éléments dans la barre de navigation'
 => 'Weitere Objekt im Navigationsmenü',
'de l\'élément courant à coté de la description'
 => 'das aktuelle Objekt neben der Beschreibung',
'Champs avec beaucoup de texte (généralement la description) à mettre avant les informations'
 => 'Felder mit viel Text (meistens Beschreibungen) sollten vor den Informationen gesetzt werden',
'Information avec un lien vers un filtre (quand on clique dessus, GCweb affichera tous les éléments ayant cette valeur)'
 => 'Information mit einem Link zu einen Filter (wenn man drauf klickt wird GCweb alle Objekte mit diesem Wert anzeigen)',
'Label'
 => 'Label',
'Champ'
 => 'Feld',
'Autres informations sans lien'
 => 'Weitere Informationen ohne Link',
'Liste de lien'
 => 'Linkliste',
'url'
 => 'url',
'Autres champs avec beaucoup de texte'
 => 'Andere Felder mit viel Text',
'Cliquez dans le coin inférieur droit pour voir une autre image'
 => 'Klicken Sie in die rechte untere Ecke, um ein anderes Bild anzuzeigen.',
'Suivant'
 => 'Nächste',
'Cette page liste les éléments de la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#list">plus d\'explications</a>)'
 => 'Diese Seite listet die Objekte der Sammlung auf. (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#list">mehr Informationen</a>)',
'Certainement un chiffre entre 0 et 10'
 => 'Sicher eine Zahl zwischen 0 und 10',
'Image réprésentative'
 => 'Beispielsbild',
'Taille de cette image'
 => 'Grösse des Bildes',
'Information avec un lien vers un filtre (quand on clique dessus, GCweb affichera tous
            les éléments ayant cette valeur). Ne sélectionnez pas trop d\'éléments car le place
            est limitée.'
 => 'Information mit einem Link auf einen Filter (wenn man drauf klickt, zaigt GCWeb
            alle Objekte mit diesem Wert an): Wählen sie nicht zuviele Elemente aus
            da der Platz begrenzt ist.',
'Autres informations sans lien (idem pour le nombre d\'éléments)'
 => 'Andere Informationen ohne Link (gleiche Objektanzahl)',
'Plus d\'info'
 => 'Weitere Infos',
'Affiche tous les élément de la collection sur une seule page (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#listall">plus d\'explications</a>)'
 => 'Zeigt alle Objekte einer Sammlung auf einer Seite an (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#listall">mehr Informationen</a>)',
'<strong>Enregistrer simplement cette page</strong> (il n\'y a aucun champ à compléter)'
 => '<strong>Diese Seite einfach nur abspeichern</strong> (es gibt kein Feld welches ausgefüllt werden muss)',
'Attention! Les modèles "menu" et "list" doivent être crées avant cette page'
 => 'Achtung! Die Vorlage "menu" und "list" müssen vor dieser erstellt werden',
'Veuillez patienter le chargement de cette page peut être long'
 => 'Bitte warten, das Laden dieser Seite kann länger dauern',
'masquer ce message'
 => 'Diesen Beitrag ausblenden',
'Cette partie s\'intégre dans la page d\'accueil et propose un petit aperçu de la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#main">plus d\'explications</a>)'
 => 'Dieser Teil ist in die Homepage integriert und bietet einen kleinen Überblick über die Sammlung (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#main">mehr Informationen</a>)',
'L\'aperçu de cette page ne s\'affiche pas correctement dans le générateur
    (alignement des images, informations complémentaires affichée de façon
    permanente ...) ceci est "normal" car la page d\'accueil n\'est pas affichée
    dans sont intégralitée'
 => 'Die Vorschau dieser Seite wird im Generator nicht korrekt angezeigt
    (Ausrichtung der Bilder, ergänzende Informationen dauerhaft
    angezeigt ...) Dies ist "normal", weil die Homepage
    nicht ganz angezeigt wird',
'Taille max de celle-ci'
 => 'Maximale Größe von diesem',
'Information avec un lien vers un filtre (quand on clique dessus, GCweb affiche tout
            les éléments ayant cette valeur)'
 => 'Information mit einem Link auf einen Filter (wenn man drauf klickt, zeigt GCweb
            alle Objekte mit diesem Wert an)',
'Menu de gauche commun à la plupart des pages (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#menu">plus d\'explications</a>)'
 => 'Linkes Menü für die meisten Seiten (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#menu">mehr Informationen</a>)',
'Section "tri des éléments"'
 => 'Bereich "Sortierobjekte"',
'champ selon lequel trier'
 => 'Sortierfeld',
'Section "recherche rapide"'
 => 'Bereicht "Schnellsuche"',
'Label du champ de recherche'
 => 'Label des Suchfeldes',
'champ dans lequel chercher'
 => 'Feld in dem gesucht werden soll',
'Navigation'
 => 'Navigation',
'Page d\'accueil'
 => 'Startseite',
'Toute la collection'
 => 'Komplette Sammlung',
'Filtres "nuages"'
 => '"Cloud" Filter',
'Classer par'
 => 'Sortieren nach',
'Chercher'
 => 'Suchen',
'dans'
 => 'in',
'Recherche avancée'
 => 'Erweiterte Suche',
'S\'abonner'
 => 'Abonnieren',
'S\'abonner au flux RSS'
 => 'RSS-Feed abonnieren',
'Éléments affichés'
 => 'Angezeigte Elemente',
'Informations'
 => 'Informationen',
'aucun élément affiché'
 => 'kein Objekt angezeigt',
'un élément affiché'
 => 'ein Objekt angezeigt',
'%d éléments affichés'
 => '%d Objekte angezeigt',
'aucun élément filtré'
 => 'kein Objekt gefiltert',
'un élément filtré'
 => 'ein Objekt gefiltert',
'%d éléments filtrés'
 => '%d Objekte gefiltert',
'Collection mise à jour<br />le %s</li>'
 => 'Sammlung aktualisiert <br />am %s</li>',
'%e/%m/%y'
 => '%e/%m/%y',
'Cette page affiche toutes les images de la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#mosaique">plus d\'explications</a>)'
 => 'Diese Seite zeigt alle Bilder in der Sammlung an (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#mosaique">mehr Informationen</a>)',
'L\'image n\'est pas obligatoir mais ce type d\'affichage n\'a aucun intéret sans'
 => 'Das Bild wird nicht benötigt, aber diese Art der Anzeiger ist ohne nicht interessant',
'Hauteur'
 => 'Höhe',
'<code>120</code> est conseillé pour des images plus haute que large'
 => '<code>120</code> wird für Bilder empfohlen, die höher als breit sind',
'Largeur standard'
 => 'Standardbreite',
'<code>80</code> est conseillé. Les images plus large seront rognées, 2 bandes noire compléteront les plus étroite'
 => '<code>80</code> wird empfohlen. Größere Bilder werden abgeschnitten, 2 schwarze Balken werden die kleinere angehängt',
'Information avec un lien vers un filtre (affiché lors du survole de l\'élément'
 => 'Informationen mit einem Link zu einem Filter (Wird angezeigt, wenn der Zeiger auf das Objekt zeigt',
'Cette page liste tous les éléments de la collection sous la forme d\'un tableau
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#onlydesc">plus d\'explications</a>)'
 => 'Diese Seite listet alle Objekte in der Sammlung als Tabelle auf.
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#list">mehr Informationen</a>)',
'Dimension maximum'
 => 'Maximale Größe',
'ex:'
 => 'zB:',
'1ère colonne (généralemment le "titre" de l\'élément'
 => '1. Spalte (normalerweise der "Titel" des Elements',
'saisie obligatoire'
 => 'erforderliches Feld',
'Nom de la colonne'
 => 'Spaltenname',
'Champ à afficher'
 => 'Anzeigefeld',
'2ème colonne'
 => '2. Spalte',
'3ème colonne'
 => '3. Spalte',
'4ème colonne (ça commence à faire beaucoup de colonne sur des écrans de faible résolution)'
 => '4. Spalte (eine Menge Spalten mit niedriger Auflösung zu machen)',
'5ème colonne (au-delà, c\'est trop !)'
 => '5. Spalte (darüber hinaus ist es zu viel!)',
'Champs à afficher'
 => 'Anzeigefelder',
'Les flux de syntication RSS permettent aux visiteurs de suivre vos récente acquisition
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#rss">Plus d\'explications</a>)'
 => 'RSS-Feeds ermöglichen es den Besuchern, Ihre neusten Erwerbe zu verfolgen.
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#list">mehr Informationen</a>)',
'L\'aperçu ne fonctionne pas. Il vous faut enregistrer le modèle puis tester avec
    votre lecteur de flux habituel'
 => 'Die Vorschau funktioniert nicht. Sie müssen die Vorlage speichern und dann
    mit Ihrem üblichen Feed-Reader testen',
'Conseillé : 1000x120, les proportions seront conservées'
 => 'Empfohlen: 1000x120, die Proportionen bleiben erhalten',
'Catégorie(s)'
 => 'Kategorie (n)',
'Source'
 => 'Quelle',
'source'
 => 'Quelle',
'Informations diverses'
 => 'Weitere Informationen',
'Texte long (description)'
 => 'Langtext (Beschreibung)',
'description'
 => 'Beschreibung',
'Cette page permet de faire des recherches avancées dans la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#search">plus d\'explications</a>)'
 => 'Diese Seite ermöglicht eine erweiterte Suchen in der Sammlung (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#search">mehr Informationen</a>)',
'Champ pour les recherches de type chaine de caractère (contient le mot, est ..)'
 => 'Feld für eine String Suche (enthält Wort, ist ..)',
'champ'
 => 'Feld',
'Champ pour les recherches de type liste (affichera une liste multichoix permetant de choisir un ou plusieurs éléments)'
 => 'Feld für eine Listensuche (zeigt eine Pulldownliste die eine Auswahl eines oder mehrerer Objekte erlaubt)',
'Champ pour les recherches de type numérique (permet de faire des recherches dans un intervale)'
 => 'Feld für eine numerische Suche (kleiner als, größer als ..)',
'Multicritères'
 => 'Mehrfachkriterien',
'Les critères vides seront ignorés.'
 => 'Leere Felder werden ignoriert.',
'Les dates peuvent être entrée sous la forme %s, 2007 (considérer comme 1er janvier 2007), ...'
 => 'Termine können im Format %s, 2007 (Entspricht dem 1. Januar 2007) eingegeben werden, ...',
'Dans les listes à choix multiples pour séléctionner plusieurs éléments maintenez pressé la touche contrôle.'
 => 'Für eine Mehrfachauswahl der Optionen in einer Liste, halten Sie die STRG-Taste gedrückt.',
'Requête'
 => 'Abfrage',
'Il est possible de réaliser une requête composée de plusieurs conditions afin d\'effectuer une recherche encore plus précise'
 => 'Es ist möglich, eine Abfrage aus mehreren Bedingungen durchzuführen, um eine noch genauere Suche durchzuführen.',
'Chercher dans'
 => 'Suche in',
'toute la base'
 => 'gesamten Daten',
'les resultats précédents'
 => 'die bisherigen Ergebnisse',
'Champs contenant du texte'
 => 'Textfelder',
'contient'
 => 'enthält',
'ne contient pas'
 => 'enthält nicht',
'est (chaîne exacte)'
 => 'Ist (genaue Zeichenfolge)',
'n\'est pas (chaîne exacte)'
 => 'Ist nicht (genaue Zeichenfolge)',
'Champs numériques et dates'
 => 'Zahlenfelder und Termine',
'égal'
 => 'gleich',
'différent'
 => 'unterschiedlich',
'plus petit'
 => 'kleiner',
'plus petit ou égal'
 => 'kleiner oder gleich',
'plus grand'
 => 'größer',
'plus grand ou égal'
 => 'größer oder gleich',
'Ajouter la condition'
 => 'Zustand hinzufügen',
'Permet d\'exporter le liste sous forme de fichier texte brut (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#txt">plus d\'explications</a>)'
 => 'Erlaubt es Ihnen, die Liste als Klartext-Datei zu exportieren (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#txt">mehr Informationen</a>)',
'Lors de la prévisualisation et de l\'enregistrement des messages du type %s ainsi que des "%s" et "~%s" vont s\'afficher <strong>ceux-ci n\'ont aucune importance</strong>'
 => 'Wenn bei der Vorschau oder dem  Speichern von %s Nachrichten, "%s" oder "~%s" angezeigt wird, <strong>können diese ignoriert werden</strong>',
'Ligne type (certain champs peuvent être vides, les valeurs des menus déroulants et le textes ne seront pas séparés par un espace si vous n\'en avez pas mis.)'
 => 'Zeilentyp (einige Felder können leer sein, die Werte der Dropdown-Menüs und der Text werden nicht durch ein Leerzeichen getrennt, wenn man sie nicht gesetzt hat).',
));
?>

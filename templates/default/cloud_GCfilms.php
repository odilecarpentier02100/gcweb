<?php
/*
 *      this file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="cloud">
        <div class="element">
            <div id="cloud_genre" class="box">
                <h2 id="genre"><?php echo __('Genres') ?></h2>
                <?php aff_cloud('genre') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_director" class="box">
                <h2 id="director"><?php echo __('Réalisateurs') ?></h2>
                <?php aff_cloud('director') ?>
            </div>
        </div>

        <div class="element">
            <div id="coud_actors" class="box">
                <h2 id="actors"><?php echo __('Acteurs') ?></h2>
                <?php aff_cloud('actors_without_roles') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_coutry" class="box">
                <h2 id="country"><?php echo __('Pays') ?></h2>
                <?php aff_cloud('country') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_year" class="box">
                <h2 id="year"><?php echo __('Années de sortie') ?></h2>
                <?php aff_cloud('year') ?>
            </div>
        </div>
    </div>
</div>

/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*** Variable de configuration ***/
var calque_index = 10;  //propriétée z-index de départ
var incrtps = 40;       //milli seconde entre chaque incrémentation
var incralpha = 0.1;    //incrémentation de l'opcité
var maxalpha = 0.8;     //opacité max (mettre un valeur < 1 si on veux avoir un leger effet de trasparence à la fin

/*** Intialisation variable ***/
var oldmenu = '';
var blockmenu = '';
var max_i = 1 / incralpha + 1;

/*** Fonctions ***/

function show(id) {
    calque_index ++;
    document.getElementById(id).style.zIndex = calque_index;

    var tps = 0;
    setTimeout("hide2show_" + id + " = 1;\
                document.getElementById('" + id + "').style.opacity = alpha_" + id + ";\
                document.getElementById('" + id + "').style.visibility = 'visible';\
                if (typeof(alpha_" + id + ") == 'undefined') {\
                    var alpha_" + id + " = 0;\
                } else if (alpha_" + id + " < 0) {\
                    var alpha_" + id + " = 0;\
                };");

    for (i = 0; i < max_i; i ++) {
        setTimeout("if (hide2show_" + id + " == 1 & alpha_" + id + " <= " + maxalpha + ") {\
                        alpha_" + id + " = alpha_" + id + " + " + incralpha + ";\
                        document.getElementById('" + id + "').style.opacity=alpha_" + id + ";\
                    }",tps);
        tps += incrtps;
    }
}

function hide(id) {
    var hide_id = document.getElementById(id);
    if (hide_id) {
        hide_id.style.zIndex = calque_index;
        calque_index ++;

        var tps = 0;
        setTimeout("hide2show_" + id + " = 0;\
                    if (typeof(alpha_" + id + ") == 'undefined') {\
                        var alpha_" + id + " = " + maxalpha + ";\
                    } else if (alpha_" + id + " > " + maxalpha + ") {\
                        var alpha_" + id + " = " + maxalpha + "\
                    }");

        for (i = 0; i < max_i; i ++) {
            setTimeout("if (hide2show_" + id + " == 0) {\
                            alpha_" + id + " = alpha_" + id + " - " + incralpha + ";\
                            if (alpha_" + id + " > " + incralpha + ") {\
                                document.getElementById('" + id + "').style.opacity = alpha_" + id + ";\
                            } else {\
                                document.getElementById('" + id + "').style.visibility = 'hidden'; \
                            }\
                        }",tps);
            tps += incrtps;
        }
    }
}

function showMenu(menu) {
    if (oldmenu !== '') {
        hide(oldmenu);
        oldmenu = '';
    } else {
        show(menu);
        oldmenu = menu;
    }
}
function changeMenu(menu) {
    if (oldmenu !== '') {
        hide(oldmenu);
        show(menu);
        oldmenu = menu;
    }
}
function hideoldMenu() {
    if (oldmenu !== '') {
        hide(oldmenu);
        oldmenu = '';
    }
}
function blockVisibilityMenu() {
    blockmenu = oldmenu;
    oldmenu = '';
}
function unblockVisibilityMenu() {
    oldmenu = blockmenu;
}

var oldinfo = '';
function changeInfo(info) {
    if (oldinfo === '' | oldinfo == info) {
        show(info);
    } else {
        hide(oldinfo);
        show(info);
    }
    oldinfo = info;
}

function hidedelay(id) {
    calque_index ++;
    document.getElementById(id).style.zIndex = calque_index;

    var tps = 2000;
    setTimeout("hide2show_" + id + " = 0;\
                if (typeof(alpha_" + id + ") == 'undefined') {\
                    var alpha_" + id + " = " + maxalpha + ";\
                } else if (alpha_" + id + " > " + maxalpha + ") {\
                    var alpha_" + id + " = " + maxalpha + "\
                }");

    for (i = 0; i < max_i; i ++) {
        setTimeout("if (hide2show_" + id + " == 0) {\
                        alpha_" + id + " = alpha_" + id + " - " + incralpha + ";\
                        if (alpha_" + id + " > " + incralpha + ") {\
                            document.getElementById('" + id + "').style.opacity = alpha_" + id + ";\
                        } else {\
                            document.getElementById('" + id + "').style.visibility = 'hidden'; \
                        }\
                    }",tps);
        tps += incrtps;
    }
}

var noimg = 1;
function switchimg(nbimg) {
    var alpha = 1;
    var tps = 0;
    while ( alpha >= 0.1) {
        alpha -= incralpha;
        setTimeout("document.getElementById('img" + noimg + "').style.opacity = '" + alpha + "'",tps);
        tps += incrtps;
    }
    setTimeout("document.getElementById('img" + noimg + "').style.display = 'none'",tps);

    if (noimg == nbimg) {
        noimg = 1;
    } else {
        noimg ++;
    }
    setTimeout("document.getElementById('img" + noimg + "').style.display = 'block'",tps);
    while ( alpha <= 1) {
        alpha += incralpha;
        setTimeout("document.getElementById('img" + noimg + "').style.opacity = '" + alpha + "'",tps);
        tps += incrtps;
    }
    tps += incrtps;
}


function load_img (url, parent_id) {
    /*charge l'image d'un id*/
    var imgNode = document.createElement('img');
    imgNode.setAttribute('src',url);
    document.getElementById(parent_id).appendChild(imgNode);
}


function search_addcond(out_id) {
    var and_or = document.getElementById('js_and_or').value;
    var champ = document.getElementById('js_champ').value;
    var cond = document.getElementById('js_cond').value;
    var value = document.getElementById('js_value').value;

    if (document.getElementById(out_id).value === '') {
        document.getElementById(out_id).value = and_or + champ + cond + value;
    } else {
        document.getElementById(out_id).value += ',\n' + and_or + champ + cond + value;
    }
}

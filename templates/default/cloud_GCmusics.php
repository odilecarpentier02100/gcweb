<?php
/*
 *      this file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Partie centale de la page affiche le menu et la liste de items
 *   - Cette partie est mise en cache car la consulatation de la base de
 *     la base de donnée est une opération lourde.
 *   - Les variables du tableau $info, $collec et $items peuvent être utilisées.
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="cloud">

        <div class="element">
            <div id="cloud_artist" class="box">
                <h2 id="artist"><?php echo __('Artistes') ?></h2>
                <?php aff_cloud('artist') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_genre" class="box">
                <h2 id="genre"><?php echo __('Genres') ?></h2>
                <?php aff_cloud('genre') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_origin" class="box">
                <h2 id="origin"><?php echo __('Origines') ?></h2>
                <?php aff_cloud('origin') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_composer" class="box">
                <h2 id="composer"><?php echo __('Compositeurs') ?></h2>
                <?php aff_cloud('composer') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_producer" class="box">
                <h2 id="producer"><?php echo __('Producteurs') ?></h2>
                <?php aff_cloud('producer') ?>
            </div>
        </div>

        <div class="element">
            <div id="cloud_label" class="box">
                <h2 id="label"><?php echo __('Labels') ?></h2>
                <?php aff_cloud('label') ?>
            </div>
        </div>

    </div>
</div>

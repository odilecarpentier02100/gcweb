<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centale de la page qui affiche la liste des éléments de la collection
 *   - Les variables du tableau $info, $collec et $items peuvent être utilisées.
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="onlydesc">

       <div class="element">
            <div class="box">
                <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menuOptionsAff.php'; ?>

                <table>
                    <tr class='title'>
                        <th>
                            <?php echo __('Titre') ?>
                            <a <?php if (isSortKey('titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleASC') ?>">↓</a><a <?php if (isSortKey('titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleDSC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Réalisateur') ?>
                            <a <?php if (isSortKey('directorASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('directorASC') ?>">↓</a><a <?php if (isSortKey('directorDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('directorDSC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Année') ?>
                            <a <?php if (isSortKey('yearASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('yearASC') ?>">↓</a><a <?php if (isSortKey('yearDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('yearDSC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Synopsis') ?>
                            <a <?php if (isSortKey('synopsisASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('synopsisASC') ?>">↓</a><a <?php if (isSortKey('synopsisDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('synopsisDSC') ?>">↑</a>
                        </th>
                    </tr>

                    <?php
                    $paire = False;
                    foreach ($bdd as $item) {
                        if ($paire)     {   $paire=False;   $class='paire';     }
                        else            {   $paire=True;    $class='impaire';   }
                        ?>
                        <tr id="id_<?php aff($item['id']) ?>" class="line_<?php echo $class ; if(test($item['borrower']) & (convert($item['borrower']) != 'none')) echo ' lent lent_'.$info['lang'];?>">
                            <th>
                                <a class="title" href="<?php aff_hrefitem($item) ?>" onmouseover="javascript:load_img('<?php echo URL_GCWEB.'index.php'; aff_image($item['image'],'auto',120) ?>', 'parent_image_<?php aff($item['id'])?>'); show('parent_image_<?php aff($item['id'])?>')" onmouseout="javascript:hide('parent_image_<?php aff($item['id'])?>');">
                                    <?php aff($item['title']);
                                    if (test($item['original']))
                                        echo "
                                    <span class='aka'>(".convert($item['original']).")</span>";
                                ?></a>
                                <span id="parent_image_<?php aff($item['id']) ?>" class="image"></span>
                            </th>
                            <td><?php aff_filter('director==',$item['director'],'',', ','','onlydesc') ?></td>
                            <td><?php aff_filter('year==',$item['year'],'',', ','','onlydesc') ?></td>
                            <td>
                                <?php $synopsis = strip_tags(convert($item['synopsis'])); ?>
                                <a href="<?php aff_hrefitem($item) ?>" title="<?php echo $synopsis ?>">
                                <?php
                                if (strlen($synopsis) < 160)
                                    echo $synopsis;
                                else {
                                    $pos = strpos($synopsis,' ',150);
                                    if ($pos)   echo substr($synopsis,0,$pos).' ...';
                                    else        echo $synopsis;
                                }
                                ?>
                                </a>
                            </td>
                        </tr>
                    <?php }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
